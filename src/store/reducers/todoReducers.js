import {ADD_TODO, REMOVE_TODO, TOGGLE_TODO} from "../actions/types";

const initialState =  [{id:1, text: 'test', completed: false}]

const todo =(state = initialState, action) =>{
    switch (action.type){
        case ADD_TODO:
            return [
                ...state,
                {
                    id: action.id,
                    text: action.text,
                    completed: false
                }
            ];
        case TOGGLE_TODO:
            return state.map(todo =>
                todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
            );


        case REMOVE_TODO:
            return state.filter((item) => item.id !== action.payload.id);


        default:
            return state;
    }

}

export default todo