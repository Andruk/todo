import {SHOW_ACTIVE, SHOW_ALL, SHOW_COMPLETED} from "../actions/types";


const Filter = (todo = [] , action) => {
 switch (action.type){
     case SHOW_COMPLETED:
         return action.payload;
     case SHOW_ALL:
         return todo, action.payload;
     case SHOW_ACTIVE:
         return action.payload;
     default : return todo
 }

}



export  default Filter