import {combineReducers, createStore} from "redux";
import todo from "./reducers/todoReducers";

import { composeWithDevTools } from 'redux-devtools-extension';
import visibilityFilter from "./reducers/filterReducer";

const store = createStore(combineReducers({
    visibilityFilter,
    todo,


}),composeWithDevTools())


export default store