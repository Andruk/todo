import {ADD_TODO, REMOVE_TODO, SHOW_ACTIVE, SHOW_ALL, SHOW_COMPLETED, TOGGLE_TODO,} from "./types";
import {v4 as uuid} from 'uuid'



export const addTodo = text => ({
    type: ADD_TODO,
    id: uuid(),
    text
});

export const toggleTodo = id => ({
    type: TOGGLE_TODO,
    id
});



export const removeTodo = id => ({
    type: REMOVE_TODO,
    payload: {
        id,
    },
});

export const showAllTodo = (state) =>{

    return {
        type: SHOW_ALL,
        payload: state.todo
    }
};

export const showCompletedTodo = (state) =>{
    let completedTodo = state.todo.filter( (item) => item.completed);
    return {
        type: SHOW_COMPLETED,
        payload: completedTodo
    }
};

export const showActiveTodo = (state) =>{
    let activeTodo =  state.todo.filter( item => !item.completed)
    return {
        type: SHOW_ACTIVE,
        payload: activeTodo
    }
};



