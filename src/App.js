
import FilterButtons from "./components/FilterButtons/FilterButtons";
import {useSelector} from "react-redux";
import {BrowserRouter as Router} from "react-router-dom";
import { Route, Switch } from 'react-router'
import Nav from "./components/Nav";
import TodoForm from "./components/TodoForm/TodoForm";
import TodoList from "./components/TodoList/TodoList";


function App() {
const todo = useSelector(state=>state)
  return (
      <Router>
          <Nav />

        <Switch>
        <Route path='/todo'>
       <TodoForm/>
       <TodoList />
        </Route>
        <Route path='/history' >
          <FilterButtons />
          {todo.visibilityFilter.map(({text})=><h1>{text}</h1>)}
        </Route>

        </Switch>
    </Router>
  );
}

export default App;
