import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {showActiveTodo, showAllTodo, showCompletedTodo} from "../../store/actions/actionCreators";


const FilterButtons = () =>{
const dispatch = useDispatch()
    const todo = useSelector(state => state)


        return (

            <span >
                <button onClick={()=>dispatch(showAllTodo(todo))} > all</button>
                <button onClick={()=>dispatch(showCompletedTodo(todo))} > done</button>
                <button onClick={()=>dispatch(showActiveTodo(todo))}> active</button>
            </span>

        );
}

export default FilterButtons;