import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import './TodoList.css'
import {removeTodo, toggleTodo} from "../../store/actions/actionCreators";


const TodoList = () => {
    const todo = useSelector(state => state)
    console.log(todo.Filter)

    const dispatch = useDispatch()

        return (
            <>
            {/*{todo.Filter.map(({text})=><h1>{text}</h1>)}*/}
            <ul>
                {todo.todo.map(({id,text,completed})=>{
                    return(
                    <li key = {id}>
                        <label className={completed ? 'completed' : undefined}>
                            <input  onChange={()=>dispatch(toggleTodo(id))}  type='checkbox' checked={completed}/>
                            {text}
                        </label>
                        <button onClick={()=>dispatch(removeTodo(id))}>Remove</button>
                    </li>)
                })}
            </ul>
            </>
        );
}

export default TodoList;