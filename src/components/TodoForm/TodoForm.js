import React,{useState} from 'react';
import {useDispatch} from "react-redux";
import {addTodo} from "../../store/actions/actionCreators";


const TodoForm = () => {
    const [inpVal, setInpVal] = useState('')
    const dispatch = useDispatch()
    const handleSubmit = (e) =>{
        e.preventDefault()
        dispatch(addTodo(inpVal))
        setInpVal('')
    }

        return (
            <form onSubmit={handleSubmit}>
                <input value={inpVal} onChange={e=>setInpVal(e.target.value)} type='text' placeholder='Add new todo'/>
                <button  type='submit'>Add</button>
            </form>

        );
}

export default TodoForm;